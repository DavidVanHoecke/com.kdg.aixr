﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Linq;

public class AIXR : EditorWindow
{
    //Rects
    Rect headerSection;
    Rect titleSection;

    Rect AIButtonsSection;
    Rect propertiesSection;

    Rect AIAgentSection;
    Rect AIpropertiesSection;
    Rect eventsSection;
    Rect showGraphButtonSection;

    //Textures
    Texture2D headerSectionTexture;
    Texture2D gatheringDataButtonTexture;
    Texture2D assessButtonTexture;

    //Styles
    GUIStyle h1Style = new GUIStyle();
    GUIStyle gatheringDataButtonStyle = new GUIStyle();
    GUIStyle assessButtonStyle = new GUIStyle();
    GUIStyle m_LinkStyle = new GUIStyle();

    //Layout Variables
    const int margin = 10;
    Vector2 eventsScrollPosition = new Vector2(0, 0); //Position of scrollbar for events

    //Variables for AI agent
    public AIAgent aiagent; //Active AI agent

    List<AIAgent> aiagents = new List<AIAgent>(); //List of all AI agents (int AI System > AI Agents)
    static public string[] aiagentNames; //List of AI Agent names (starts with Select and ends with Add)
    public int selectedAIAgent = 0; //Index of selected item in dropdown menu

    SerializedProperty onAboveThresholdProperty;
    SerializedProperty onWithinThresholdProperty;
    SerializedProperty onBelowThresholdProperty;

    SerializedObject onAboveThresholdSerializedObject;
    SerializedObject onWithinThresholdSerializedObject;
    SerializedObject onBelowThresholdSerializedObject;

    string assessingfilePath;
    string outputFolderPathName;

    //Variables for creating AIAgents
    GameObject aiSystem;
    Transform aiagentsContainer;
    AIAgent aiagentPrefab;
    string message;
    bool training;

    [MenuItem("Window/AIXR")]
    //Opens window and sets minimum size
    static void OpenWindow()
    {
        AIXR window = (AIXR)GetWindow(typeof(AIXR));
        window.minSize = new Vector2(600, 450);
        window.Show();
    }

    void OnEnable()
    {
        aiagentPrefab = Resources.Load<AIAgent>("AI Agent"); //Look for AI agent prefab
        outputFolderPathName = Path.Combine(Application.persistentDataPath, "AIAgent Output");
        AIAgent.OnStartTrain += StartMessage;
        AIAgent.OnCompleteTrain += EndMessage;
    }

    void OnDisable()
    {
        AIAgent.OnStartTrain -= StartMessage;
        AIAgent.OnCompleteTrain -= EndMessage;
    }
    private void StartMessage()
    {
        message = "Training in progress";
        Repaint();
    }
    private void EndMessage()
    {
        message = "Training ended";
        Repaint();
    }
    void initStyles()
    {
        //Headersection (background)
        headerSectionTexture = new Texture2D(1, 1);
        headerSectionTexture.SetPixel(0, 0, Color.black);
        headerSectionTexture.Apply();

        //h1
        h1Style.normal.textColor = Color.white;
        h1Style.fontSize = 40;
        h1Style.alignment = TextAnchor.MiddleCenter;

        //Gathering data button
        gatheringDataButtonTexture = Resources.Load<Texture2D>("icons/GatheringDataButton");
        gatheringDataButtonStyle.normal.background = gatheringDataButtonTexture;
        gatheringDataButtonStyle.fixedHeight = 50;
        gatheringDataButtonStyle.fixedWidth = 75;
        gatheringDataButtonStyle.margin.left = (int)(position.width - (gatheringDataButtonStyle.fixedHeight * 2)) / 3;

        //Assess button
        assessButtonTexture = Resources.Load<Texture2D>("icons/AssessButton");
        assessButtonStyle.normal.background = assessButtonTexture;
        assessButtonStyle.fixedHeight = gatheringDataButtonStyle.fixedHeight;
        assessButtonStyle.fixedWidth = gatheringDataButtonStyle.fixedWidth;
        assessButtonStyle.margin.left = (int)(position.width - (gatheringDataButtonStyle.fixedWidth * 2)) / 3;

        // link button (delete files)
        m_LinkStyle = new GUIStyle(EditorStyles.label);
        m_LinkStyle.wordWrap = false;
        m_LinkStyle.padding = new RectOffset(0, 0, 20, 10);
        // Match selection color which works nicely for both light and dark skins
        m_LinkStyle.normal.textColor = new Color(0x00 / 255f, 0x78 / 255f, 0xDA / 255f, 1f);
        m_LinkStyle.stretchWidth = false;
        m_LinkStyle.fontStyle = FontStyle.Bold;
    }

    void DrawLayouts()
    {
        //Header section
        headerSection.x = 0;
        headerSection.y = 0;
        headerSection.width = position.width;
        headerSection.height = 50;

        GUI.DrawTexture(headerSection, headerSectionTexture);

        //Title section
        titleSection.x = 0;
        titleSection.y = 0;
        titleSection.width = position.width;
        titleSection.height = h1Style.fontSize + margin;

        //AI buttons section
        AIButtonsSection.x = (position.width - gatheringDataButtonStyle.fixedWidth - assessButtonStyle.fixedWidth) / 3;
        AIButtonsSection.y = headerSection.height + 15;
        AIButtonsSection.width = position.width - (position.width - gatheringDataButtonStyle.fixedWidth - assessButtonStyle.fixedWidth) * 2 / 3;
        AIButtonsSection.height = gatheringDataButtonStyle.fixedHeight + 15;

        //Show graph button part 1 (showGraphButtonSection.height needs to be set to set others heights)
        showGraphButtonSection.height = 70 + margin;

        //Properties section
        propertiesSection.x = margin;
        propertiesSection.y = AIButtonsSection.y + AIButtonsSection.height;
        propertiesSection.width = position.width - propertiesSection.x * 2;
        propertiesSection.height = position.height - propertiesSection.y - showGraphButtonSection.height - margin * 2;

        //AI agent section
        AIAgentSection.x = 0;
        AIAgentSection.y = 0;
        AIAgentSection.width = propertiesSection.width;
        AIAgentSection.height = 50;

        //AI properties section
        AIpropertiesSection.x = 0;
        AIpropertiesSection.y = AIAgentSection.height;
        AIpropertiesSection.width = propertiesSection.width;
        AIpropertiesSection.height = 70;

        //Events section
        eventsSection.x = 10;
        eventsSection.y = AIpropertiesSection.height + AIAgentSection.height;
        eventsSection.width = AIpropertiesSection.width - eventsSection.x * 2;
        eventsSection.height = propertiesSection.height - eventsSection.y - showGraphButtonSection.height;

        //Show graph button part 2
        showGraphButtonSection.x = position.width / 3;
        showGraphButtonSection.y = propertiesSection.y + propertiesSection.height;
        showGraphButtonSection.width = position.width / 3;
    }

    //On every "interaction" with the window: calculate layout and draw styles, header and body)
    void OnGUI()
    {
        initStyles();
        DrawLayouts();
        DrawHeader();
        DrawBody();
    }

    void DrawHeader()
    {
        GUILayout.BeginArea(titleSection);
        GUILayout.Label("AIXR", h1Style);
        GUILayout.EndArea(); //Title section
    }

    void DrawBody()
    {
        GUILayout.BeginArea(AIButtonsSection);
        GUILayout.BeginHorizontal();
        //Gather data button
        if (aiagent) //Check for an active AI agent
        {
            if (aiagent.mode != AIAgent.Mode.Assessing) //Check if agent isn't assessing
            {
                if (aiagent.mode != AIAgent.Mode.GatherTrainingData) //If the agent isn't gathering data show gathering button
                {
                    gatheringDataButtonStyle.normal.background = Resources.Load<Texture2D>("icons/GatheringDataButton");
                    if (GUILayout.Button(new GUIContent("", "Press to start gathering data"), gatheringDataButtonStyle))
                    {
                        //When record button is clicked (gather data)
                        aiagent.mode = AIAgent.Mode.GatherTrainingData;
                    }
                }
                else //If the agent is gathering data show stop gathering button
                {
                    gatheringDataButtonStyle.normal.background = Resources.Load<Texture2D>("icons/BusyGatheringDataButton");
                    if (GUILayout.Button(new GUIContent("", "Press to stop gathering data"), gatheringDataButtonStyle))
                    {
                        //When "is recording" button is clicked (stop gathering data)
                        aiagent.mode = AIAgent.Mode.Idle;
                    }
                }
            }
            else //If the agent is assessing show gray button
            {
                gatheringDataButtonStyle.normal.background = Resources.Load<Texture2D>("icons/GatheringDataButtonUnable");
                if (GUILayout.Button(new GUIContent("", "Can't gather data while assessing"), gatheringDataButtonStyle))
                {
                    //When grey record button data is clicked (unable to gather data)
                }
            }
        }
        else //If there isn't an agent show grey button
        {
            gatheringDataButtonStyle.normal.background = Resources.Load<Texture2D>("icons/GatheringDataButtonUnable");
            if (GUILayout.Button(new GUIContent("", "Select an AI agent to start gathering data"), gatheringDataButtonStyle))
            {
                //When grey record button data is clicked (unable to gather data)
            }
        }

        //Assess button
        if (aiagent) //Check for an active AI agent
        {
            assessingfilePath = System.IO.Path.Combine(outputFolderPathName, aiagent.identifier + "-assessing.csv"); //Makes path to assessing file

            if (aiagent.mode != AIAgent.Mode.GatherTrainingData && File.Exists(assessingfilePath)) //Check if agent isn't assessing and the assessing file exists
            {
                if (aiagent.mode != AIAgent.Mode.Assessing) //If the agent isn't assessing show assessing button
                {
                    assessButtonStyle.normal.background = Resources.Load<Texture2D>("icons/AssessButton");
                    if (GUILayout.Button(new GUIContent("", "Press to stop assessing"), assessButtonStyle))
                    {
                        //When play button is clicked (assess)
                        aiagent.mode = AIAgent.Mode.Assessing;
                    }
                }
                else //If the agent is assessing show stop assessing button
                {
                    assessButtonStyle.normal.background = Resources.Load<Texture2D>("icons/PauseButton");
                    if (GUILayout.Button(new GUIContent("", "Press to start assessing"), assessButtonStyle))
                    {
                        //When pause button is clicked (stop assessing)
                        aiagent.mode = AIAgent.Mode.Idle;
                    }
                }
            }
            else //If the agent is assessing show gray button
            {

                assessButtonStyle.normal.background = Resources.Load<Texture2D>("icons/AssessButtonUnable");
                if (aiagent.mode == AIAgent.Mode.GatherTrainingData) //If the agent is gatherring data adjust tooltip text
                {
                    if (GUILayout.Button(new GUIContent("", "Can't asses while gathering data"), assessButtonStyle))
                    {
                        //When grey play button is clicked (unable assess)
                    }
                }
                else if (!File.Exists(assessingfilePath)) //If the assessing file doesn't exist adjust tooltip text
                {
                    if (GUILayout.Button(new GUIContent("", "You can't asses, gather data first"), assessButtonStyle))
                    {
                        //When grey play button is clicked (unable assess)
                    }
                }
            }
        }
        else //If there isn't an agent show grey button
        {
            assessButtonStyle.normal.background = Resources.Load<Texture2D>("icons/AssessButtonUnable");
            if (GUILayout.Button(new GUIContent("", "Select an AI agent to start gathering data"), assessButtonStyle))
            {
                //When grey play button is clicked (unable assess)
            }
        }

        GUILayout.EndHorizontal();
        GUILayout.EndArea(); //AI button section
        GUILayout.BeginArea(propertiesSection);
        GUILayout.BeginArea(AIAgentSection);

        FillAIAgentNames(); //Fill the agent names for the dropdown menu options
        selectedAIAgent = EditorGUILayout.Popup("AI agent", selectedAIAgent, aiagentNames); //Dropdown menu to choose AI agents

        if (selectedAIAgent == aiagentNames.Length - 1) //When the add option is pressed
        {
            ChooseAName.OpenWindow(); //Open the choose AI agent name window

            selectedAIAgent = 0; //Set the dropdown menu back to "Select"
        }
        else if (selectedAIAgent != 0) //If an AI agent is selected set it as the active AI agent
        {
            aiagent = aiagents[selectedAIAgent - 1];

        }
        else //When the select option is pressed make active AI agent empty
        {
            aiagent = null;
        }

        GUILayout.EndArea(); //AI agent section

        if (selectedAIAgent != 0 && selectedAIAgent != aiagentNames.Length - 1)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            // delete training files button
            if (GUILayout.Button("[Click to delete current AI Agent's data files]", m_LinkStyle))
            {
                var aiAgent = aiagents[selectedAIAgent - 1];
                var identifier = aiAgent.identifier;
                Directory.GetFiles(Path.Combine(Application.persistentDataPath, "AIAgent Output"), string.Format("{0}-*", identifier)).ToList().ForEach(path =>
                {
                    File.Delete(path);
                    Debug.LogWarning(string.Format("File at path: \"{0}\" deleted.", path));
                });
            }
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.BeginArea(AIpropertiesSection);

        if (aiagent)
        {
            aiagent.ThresholdScaleFactor = EditorGUILayout.DoubleField(new GUIContent("Threshold scale", "Scale for tolerance which determines the min and max"), aiagent.ThresholdScaleFactor);
            aiagent.virtualTimeDeadZone = EditorGUILayout.FloatField(new GUIContent("Dead zone time", "Time it takes before the events are called"), aiagent.virtualTimeDeadZone);
        }
        else
        {
            EditorGUI.BeginDisabledGroup(true); //If there is no active AI agent disable Tolerance and dead zone time
            EditorGUILayout.DoubleField("Threshold scale", 0);
            EditorGUILayout.DoubleField("Dead zone time", 0);
            EditorGUI.EndDisabledGroup();
        }

        GUILayout.EndArea(); //AI properties section
        GUILayout.BeginArea(eventsSection);
        eventsScrollPosition = EditorGUILayout.BeginScrollView(eventsScrollPosition);

        if (aiagent)
        {
            //On above threshold events
            onAboveThresholdSerializedObject = new UnityEditor.SerializedObject(aiagent);
            onAboveThresholdProperty = onAboveThresholdSerializedObject.FindProperty("OnAboveThreshold");
            EditorGUILayout.PropertyField(onAboveThresholdProperty);

            onAboveThresholdSerializedObject.ApplyModifiedProperties();

            //On within threshold events
            onWithinThresholdSerializedObject = new UnityEditor.SerializedObject(aiagent);
            onWithinThresholdProperty = onWithinThresholdSerializedObject.FindProperty("OnWithinThresholds");
            EditorGUILayout.PropertyField(onWithinThresholdProperty);

            onWithinThresholdSerializedObject.ApplyModifiedProperties();

            //On below threshold events
            onBelowThresholdSerializedObject = new UnityEditor.SerializedObject(aiagent);
            onBelowThresholdProperty = onBelowThresholdSerializedObject.FindProperty("OnBelowThreshold");
            EditorGUILayout.PropertyField(onBelowThresholdProperty);

            onBelowThresholdSerializedObject.ApplyModifiedProperties();
        }

        EditorGUILayout.EndScrollView();
        GUILayout.EndArea(); //Events Section
        GUILayout.EndArea(); //Properties section

        //Show Graph Button
        GUILayout.BeginArea(showGraphButtonSection);
        bool buttonIsInactive = false;
        if (selectedAIAgent == 0) //When the Select option of the dropdown menu is active dissable show graph button 
        {
            buttonIsInactive = true;
        }
        else
        {
            if (aiagent.identifier == null) //When the active AI agent has no identifier dissable show graph button
            {
                buttonIsInactive = true;
            }
        }
        EditorGUI.BeginDisabledGroup(buttonIsInactive);
        if (GUILayout.Button(new GUIContent("Show Graph", "Open a window with the graph of the learned model")))
        {
            new LearnedModelGraph().OpenWindow(aiagent);
        }
        if (aiagent)
        {
            if (aiagent.isTraining == false)
            {
                if (GUILayout.Button(new GUIContent("Train Agent", "Train the selected AI agent")))
                {
                    if (aiagent)
                    {
                        aiagent.TrainAIAgents();
                    }
                }
            }
            EditorGUILayout.HelpBox(message, MessageType.Info);
        }
        EditorGUI.EndDisabledGroup();
        GUILayout.EndArea(); //Show graph button section
    }

    public void FillAIAgentNames()
    {
        CheckForAIAgentNames(); //Fills aiagentNames with the names of the AI agents in AI System > AI agents

        aiagentNames = new string[aiagents.Count + 2]; //The lenght of the names list must be the lenght of aiagentNames + 2 (for "Select" and "Add")

        aiagentNames[0] = "Select"; //Adds "Select" as first "name"
        for (int i = 1; i < aiagentNames.Length - 1; i++) //Adds the name of each AI agent to te "names"
        {
            aiagentNames[i] = aiagents[i - 1].gameObject.name;
        }
        aiagentNames[aiagentNames.Length - 1] = "Add..."; //Adds "Add..." as first "name"
    }

    void CheckForAIAgentNames()
    {
        aiSystem = GameObject.Find("AI System");
        if (aiSystem) //If there is an AI System gameObject check for AI agents gameobject
        {
            aiagentsContainer = aiSystem.transform.Find("AI Agents");
            if (aiagentsContainer) //If there is an AI agents gameobject clear the aiagents and starts filling again
            {
                aiagents.Clear();
                for (int i = 0; i < aiagentsContainer.childCount; i++)
                {
                    if (aiagentsContainer.GetChild(i).GetComponent<AIAgent>()) //If a gameObject isn't an AI agent give an error
                    {
                        aiagents.Add(aiagentsContainer.GetChild(i).GetComponent<AIAgent>());
                    }
                    else
                    {
                        Debug.LogError("All childeren of the \"AI\" agent gameobject must be AI Agents, no other gameobjects allowd!");
                    }
                }
            }
        }
    }

    static public void CreateNewAIgent(string nameNewAgent)
    {
        GameObject aiSystem = GameObject.Find("AI System");
        if (!aiSystem) //If there isn't an AI System gameobject create one
        {
            aiSystem = new GameObject();
            aiSystem.name = "AI System";
        }

        Transform aiagentsContainer = aiSystem.transform.Find("AI Agents");
        if (!aiagentsContainer) //If there isn't an AI Agent gameobject create one
        {
            aiagentsContainer = new GameObject().transform;
            aiagentsContainer.name = "AI Agents";
            aiagentsContainer.parent = aiSystem.transform;
        }

        AIAgent aiagentPrefab = Resources.Load<AIAgent>("AI Agent");
        AIAgent newAgent = Instantiate(aiagentPrefab, aiagentsContainer); //Create new AI agent
        newAgent.name = nameNewAgent;
        newAgent.identifier = nameNewAgent;
        newAgent.mode = AIAgent.Mode.Idle; //Set mode to Idle so it's ready for use
    }
}

public class ChooseAName : EditorWindow
{
    //Rects
    Rect labelRect;
    Rect bodyRect;

    //Styles
    GUIStyle labelStyle = new GUIStyle();
    GUIStyle errorStyle = new GUIStyle();

    //Layout Variables
    const int margin = 10;

    //Variables
    string name = "";
    string errorMsg = "";


    //Opens window and sets minimum and maximum size
    static public void OpenWindow()
    {
        ChooseAName window = (ChooseAName)GetWindow(typeof(ChooseAName));
        window.titleContent = new GUIContent("Choose a name");
        window.minSize = new Vector2(300, 100);
        window.maxSize = new Vector2(300, 100);
        window.Show();
    }

    private void initStyles()
    {
        //Label style
        labelStyle.alignment = TextAnchor.MiddleCenter;
        labelStyle.fontStyle = FontStyle.Bold;
        labelStyle.fontSize = 15;

        //ErrorStyle
        errorStyle.normal.textColor = Color.red;
    }

    private void DrawLayouts()
    {
        //Label Rect
        labelRect.x = margin;
        labelRect.y = margin;
        labelRect.width = position.width - 2 * margin;
        labelRect.height = labelStyle.fontSize + margin;

        //Body Rect
        bodyRect.x = margin;
        bodyRect.y = labelRect.height + margin;
        bodyRect.width = position.width - 2 * margin;
        bodyRect.height = position.height - labelRect.height - margin;
    }

    //On every "interaction" with the window: calculate layout and draw styles and body)
    void OnGUI()
    {
        initStyles();
        DrawLayouts();
        DrawBody();
    }

    private void DrawBody()
    {
        GUILayout.BeginArea(labelRect);
        GUILayout.Label("Enter the name for your new agent", labelStyle);
        GUILayout.EndArea();
        GUILayout.BeginArea(bodyRect);
        name = GUILayout.TextField(name);

        GUILayout.Label(errorMsg, errorStyle); //Error message for invalid name
        if (GUILayout.Button("Create"))
        {
            if (name != "")
            {
                if (!CheckIfNameAllreadyExists()) //if name doesn't exist give an empty error message and create new AI Agent with the given name
                {
                    errorMsg = "";

                    AIXR.CreateNewAIgent(name);

                    this.Close(); //When done close this window
                }
                else //If the name allready exist give error message
                {
                    errorMsg = "This name already exists";
                }
            }
            else //If the name is empty exist give error message
            {
                errorMsg = "You need to enter a name";
            }
        }
        GUILayout.EndArea();
    }

    public bool CheckIfNameAllreadyExists()
    {
        GameObject aiSystem = GameObject.Find("AI System");
        if (aiSystem) //If there is no AI System gameobject the name doesn't exist
        {
            Transform aiagentsContainer = aiSystem.transform.Find("AI Agents");
            if (aiagentsContainer) //If there is no AI Agents gameobject the name doesn't exist
            {
                if (aiagentsContainer.Find(name)) //Checks if the name exists
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
#endif