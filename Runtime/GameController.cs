using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameController : MonoBehaviour
{

    public TMP_Text text;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnStartAiAgent()
    {
        text.text += "started gathering training data" + '\n';
    }

    public void OnStopAiAgent()
    {
        text.text += "stopped gathering training data" + '\n';
    }
}
