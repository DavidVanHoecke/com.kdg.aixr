﻿//http://wiki.unity3d.com/index.php?title=EditorGraphWindow&oldid=14810

using UnityEngine;
using UnityEditor;
using System.Collections;

#if UNITY_EDITOR
public class GraphDuringTraining : EditorWindow
{
	public Color32 UpperBoundColor = Color.cyan;
	public Color32 LowerBoundColor = Color.cyan;
	public Color32 AverageValueColor = Color.blue;
	public Color32 WithinRangeColor = Color.green;
	public Color32 OutOfRangeColor = Color.red;

	public double AverageValue = 0;
	public double CurrentValue = 0;

	public static bool withinRange = true;

	Material lineMaterial;

	Graph graph = new Graph();

	[MenuItem("Window/Graph During Training")]
	static void ShowGraph()
	{
		EditorWindow.GetWindow<GraphDuringTraining>();
	}



	void OnEnable()
	{
		
		EditorApplication.update += MyDelegate;
	}

	void OnDisable()
	{
		EditorApplication.update -= MyDelegate;
	}

	void MyDelegate()
	{
		Repaint();
	}

	void CreateLineMaterial()
	{
		if (!lineMaterial)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things.
			Shader shader = Shader.Find("Hidden/Internal-Colored");
			lineMaterial = new Material(shader);
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			// Turn on alpha blending
			lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			// Turn backface culling off
			lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			// Turn off depth writes
			lineMaterial.SetInt("_ZWrite", 0);
		}
	}

	void OnGUI()
	{
		UpperBoundColor = EditorGUILayout.ColorField("Upperbound Color", UpperBoundColor);
		LowerBoundColor = EditorGUILayout.ColorField("Lowerbound Color", LowerBoundColor);
		AverageValueColor = EditorGUILayout.ColorField("Average Color", AverageValueColor);
		WithinRangeColor = EditorGUILayout.ColorField("Within Range Color", WithinRangeColor);
		OutOfRangeColor = EditorGUILayout.ColorField("Out Of Range Color", OutOfRangeColor);
		graph.channel[10]._color = UpperBoundColor;
		graph.channel[11]._color = LowerBoundColor;
		graph.channel[12]._color = AverageValueColor;
		graph.channel[13]._color = WithinRangeColor;

		AverageValue = graph.avgValue;
		CurrentValue = graph.currValue;

		AverageValue = EditorGUILayout.DoubleField("Average target state nr", System.Math.Round(AverageValue, 2));
		CurrentValue = EditorGUILayout.DoubleField("Current state nr", System.Math.Round(CurrentValue, 2));


		if (Event.current.type != EventType.Repaint)
			return;

		if (graph.channel[0] == null)
			return;

		int W = (int)this.position.width;
		int H = (int)this.position.height;

		CreateLineMaterial();
		lineMaterial.SetPass(0);

		GL.PushMatrix();
		GL.LoadPixelMatrix();

		GL.Begin(GL.LINES);

		for (int chan = 0; chan < Graph.MAX_CHANNELS-1; chan++)
		{
			Channel C = graph.channel[chan];

			if (C == null)
				Debug.Log("FOO:" + chan);

			if (!C.isActive)
				continue;

			GL.Color(C._color);

			for (int h = 0; h < Graph.MAX_HISTORY; h++)
			{
				int xPix = (W-1) - h;

				if (xPix >= 0)
				{
					float y = C._data[h];

					float y_01 = Mathf.InverseLerp(Graph.YMin, Graph.YMax, y);

					int yPix = H-(int)(y_01 * H);

					Plot(xPix, yPix);
				}
			}
		}

		for (int chan = 13; chan < Graph.MAX_CHANNELS; chan++)
		{
			
			if (withinRange)
			{
				graph.channel[13]._color = WithinRangeColor;
			}
			else
			{
				graph.channel[13]._color = OutOfRangeColor;
			}

			Channel C = graph.channel[chan];

			if (C == null)
				Debug.Log("FOO:" + chan);

			if (!C.isActive)
				continue;

			GL.Color(C._color);

			for (int h = 0; h < Graph.MAX_HISTORY; h++)
			{
				//int xPix = (W - 1) - h;
				int xPix = h;
				int yPix = 0;				

				if (xPix >= 0)
				{
					float y = C._data[h];
					float y_01 = Mathf.InverseLerp(Graph.YMin, Graph.YMax, y);
					yPix = H - (int)(y_01 * H);
					if (C._data[h] > 0 )
					{
						Plot(xPix, yPix);
					}
				}

				int previousYPix = 0;
				if (xPix >= 0 && h>0)
				{
					float y = C._data[h-1];
					float y_01 = Mathf.InverseLerp(Graph.YMin, Graph.YMax, y);
					previousYPix = H - (int)(y_01 * H);
					if (yPix != previousYPix)
					{
						if (yPix < previousYPix)
						{
							do
							{
								Plot(xPix, yPix);
								yPix += 1;
							} while (yPix < previousYPix);
						}
					}
				}				
			}
		}

		GL.End();

		GL.PopMatrix();

		EditorGUILayout.EndVertical();
	}

	// plot an X
	void Plot(float x, float y)
	{
		// first line of X
		GL.Vertex3(x - 1, y - 1, 0);
		GL.Vertex3(x + 1, y + 1, 0);

		// second
		GL.Vertex3(x - 1, y + 1, 0);
		GL.Vertex3(x + 1, y - 1, 0);
	}
}
#endif