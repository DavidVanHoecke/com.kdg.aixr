using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public Vector3 rawInputMovement = new Vector3();

    public float speed = 15f;
    float x;
    float z;

    public float gravity = -9.81f;
    public float jumpHeight = 3f;


    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public LayerMask TerrainMask;


    Vector3 velocity;
    bool isGrounded;
    bool hitTerrain;


    void Start()
    {

    }


    public void OnMovement(InputAction.CallbackContext value)
    {
        Vector2 inputMovement = value.ReadValue<Vector2>();
        rawInputMovement = new Vector3(inputMovement.x, 0, inputMovement.y);
    }

    private void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        hitTerrain = Physics.CheckSphere(groundCheck.position, groundDistance, TerrainMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        if(hitTerrain)
        {
            TrajectoryResetter tr = FindObjectOfType<TrajectoryResetter>();
            tr.ResetTrajectory();
        }

        //if (Input.GetButtonDown("Jump") && isGrounded)
        //{
        //    velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        //}

        
        Vector3 move = transform.right * rawInputMovement.x + Camera.main.transform.forward * rawInputMovement.z;

        //Vector3 move = transform.right * rawInputMovement.x + transform.forward * rawInputMovement.z;
        controller.Move(move * speed * Time.deltaTime);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }


}
