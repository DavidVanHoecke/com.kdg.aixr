﻿//http://wiki.unity3d.com/index.php?title=EditorGraphWindow&oldid=14810

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

	public class Channel
	{
		public List<float> _data = new List<float>();
		public Color _color = Color.white;
		public bool isActive = false;

		public int pointer = 0;

		public Channel(Color _C)
		{
			_color = _C;
		}

		public void Feed(float val)
		{
			for (int i = Graph.MAX_HISTORY - 1; i >= 1; i--)
				_data[i] = _data[i - 1];

			_data[0] = val;
		}

		public void FeedReversed(float val)
		{
			_data.Add(val);
		}
	}
