﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhenGettingToThisState : StateMachineBehaviour
{    
    public AIAgent aiAgent; 
    public bool mustSetAiAgentToNextState;

    public float AddValueToValueToTrack = 0;
    public float ValueToTrackValueToResetNextPhaseTo = 0;
    public float VirtualTimeToResetNextPhaseTo = 0;

    public bool mustResetTrajectory = false;
    public TrajectoryResetter trajectoryResetter;

    private void OnEnable()
    {        
        trajectoryResetter = FindObjectOfType<TrajectoryResetter>();
    }
   

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        aiAgent = animator.transform.GetComponent<AIAgent>();
        aiAgent.AddToValueToTrack(AddValueToValueToTrack);
        if (mustSetAiAgentToNextState)
        {
            //jump forward in time so the new phase starts at the same (virtual) time no matter how fast or slow the player played before this checkpoint
            aiAgent.GoToNextPhase(VirtualTimeToResetNextPhaseTo, ValueToTrackValueToResetNextPhaseTo);
        }

        if (mustResetTrajectory)
        {
            trajectoryResetter.ResetTrajectory();
        }
    }


    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
