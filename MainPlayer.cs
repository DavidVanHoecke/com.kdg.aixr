using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.Experimental.XR;
using UnityEngine.SceneManagement;

public class MainPlayer : MonoBehaviour
{

    private void Start()
    {
        if(SceneManager.GetActiveScene().name.Contains("VR"))
        {
            EnableVR(true);
        }
    }

    public void EnableVR(bool istrue)
    {
        if(istrue)
        {
            StartCoroutine( InitXR());
        }
        else
        {
            StartCoroutine(StopVR());
        }
    }

    public IEnumerator InitXR()
    {
        XRSettings.LoadDeviceByName(XRSettings.loadedDeviceName);
        XRSettings.enabled = true;

        UnityEngine.XR.Management.XRGeneralSettings.Instance.Manager.DeinitializeLoader();

        yield return UnityEngine.XR.Management.XRGeneralSettings.Instance.Manager.InitializeLoader();
        UnityEngine.XR.Management.XRGeneralSettings.Instance.Manager.StartSubsystems();
    }

    public IEnumerator StopVR()
    {
        //stop VR
        XRSettings.LoadDeviceByName(XRSettings.loadedDeviceName);
        XRSettings.enabled = false;

        UnityEngine.XR.Management.XRGeneralSettings.Instance.Manager.DeinitializeLoader();

        yield return UnityEngine.XR.Management.XRGeneralSettings.Instance.Manager.InitializeLoader();
        UnityEngine.XR.Management.XRGeneralSettings.Instance.Manager.StopSubsystems();

        //yield return null;
    }
}
