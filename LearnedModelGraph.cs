﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

public class LearnedModelGraph : EditorWindow
{
    //Rects
    Rect headerSection;
    Rect titleSection;

    Rect bodySection;
    Rect legendSection;

    Rect drawAreaSection;

    Rect sliderSection;

    //Textures
    Texture2D headerSectionTexture;
    Texture2D drawAreaSectionTexture;

    //Styles
    GUIStyle h1Style = new GUIStyle();

    GUIStyle legendLabelStyle = new GUIStyle();
    GUIStyle legendBoxStyle = new GUIStyle();

    GUISkin graphSliderSkin;
    GUIStyle graphSliderStyle = new GUIStyle();
    GUIStyle graphSliderThumbStyle = new GUIStyle();

    //Layout Variables
    const int margin = 10;

    //Variables for AI agent
    AIAgent aiagent;
    string agentName;
    Graph graph;

    Color avarageColor = Color.blue;
    Color minAndMaxColor = Color.cyan;
    Color progressWithinBoundsColor = Color.green;
    Color progressOutOfBoundsColor = Color.red;

    int graphOffset = 0;

    //Opens the window and sets teh name and minimum size of the window
    public void OpenWindow(AIAgent initAIAgent)
    {
        aiagent = initAIAgent;
        agentName = aiagent.name;
        graph = aiagent.graph;

        LearnedModelGraph window = (LearnedModelGraph)GetWindow(typeof(LearnedModelGraph));
        window.titleContent = new GUIContent(initAIAgent.name);
        window.minSize = new Vector2(230, 380);

        window.Show();

        SetChannelsActive();
    }

    void InitStyles()
    {
        //Header section
        headerSectionTexture = new Texture2D(1, 1);
        headerSectionTexture.SetPixel(0, 0, Color.black);
        headerSectionTexture.Apply();

        //h1
        h1Style.normal.textColor = Color.white;
        h1Style.fontSize = 25;
        h1Style.alignment = TextAnchor.MiddleCenter;

        //Legend label style
        legendLabelStyle.normal.textColor = Color.black;
        legendLabelStyle.fontSize = 12;
        legendLabelStyle.fontStyle = FontStyle.Bold;
        legendLabelStyle.margin.right = 25;
        legendLabelStyle.stretchWidth = false;

        //Legend box style
        legendBoxStyle.fixedWidth = 16;
        legendBoxStyle.fixedHeight = 16;
        legendBoxStyle.margin.right = (int)((legendSection.width - (int)((100 + legendLabelStyle.margin.right + (int)legendBoxStyle.fixedWidth) * 4)) / 4);

        //Draw area section
        drawAreaSectionTexture = new Texture2D(1, 1);
        drawAreaSectionTexture.SetPixel(0, 0, Color.white);
        drawAreaSectionTexture.Apply();

        //Graph slider
        graphSliderSkin = (GUISkin)Resources.Load("Skins/GraphSlider");
        graphSliderStyle = graphSliderSkin.GetStyle("horizontalslider");
        graphSliderThumbStyle = graphSliderSkin.GetStyle("horizontalsliderthumb");
    }

    void DrawLayouts()
    {
        //Header section
        headerSection.x = 0;
        headerSection.y = 0;
        headerSection.width = position.width;
        headerSection.height = 35;

        GUI.DrawTexture(headerSection, headerSectionTexture);

        //Title section
        titleSection.x = 0;
        titleSection.y = 0;
        titleSection.width = position.width;
        titleSection.height = h1Style.fontSize + margin;

        //Body section
        bodySection.x = margin;
        bodySection.y = headerSection.height;
        bodySection.width = position.width - 2 * margin;
        bodySection.height = position.height - headerSection.height - 2 * margin;

        //Legend section
        legendSection.x = 0;
        legendSection.y = 17;
        legendSection.width = bodySection.width;
        legendSection.height = 33;

        //Slider section part 1
        sliderSection.height = 50;

        //Draw Area section
        if (!aiagent)
        {
            CheckForAgent();
        }
        drawAreaSection.y = legendSection.height;
        if (graph.channel[12]._data.Count < bodySection.width)
        {
            drawAreaSection.width = position.width - margin;

            drawAreaSection.x = (bodySection.width - drawAreaSection.width) / 2;

        }
        else
        {
            drawAreaSection.width = bodySection.width;
            drawAreaSection.x = 0;
        }
        drawAreaSection.height = bodySection.height - legendSection.height - sliderSection.height - margin;

        //Slider section part 2
        sliderSection.x = 0;
        sliderSection.y = drawAreaSection.y + drawAreaSection.height;
        sliderSection.width = bodySection.width;
    }

    //On every "interaction" with the window: calculate layout and draw styles, header and body)
    void OnGUI()
    {
        InitStyles();
        DrawLayouts();
        DrawHeader();
        DrawBody();
    }

    void DrawHeader()
    {
        GUILayout.BeginArea(titleSection);
        GUILayout.Label(aiagent.gameObject.name + " graph", h1Style);
        GUILayout.EndArea(); //Title section
    }

    void DrawBody()
    {
        GUILayout.BeginArea(bodySection);
        GUILayout.BeginArea(legendSection);
        GUILayout.BeginHorizontal();

        Texture2D boxTexture = MakeTex(15, 15, avarageColor); //create texture with color of avarage
        GUILayout.Label("Average", legendLabelStyle);
        GUILayout.Box(boxTexture, legendBoxStyle);

        boxTexture = MakeTex(15, 15, minAndMaxColor); //create texture with color of minimum and maximum
        GUILayout.Label("Min and max", legendLabelStyle);
        GUILayout.Box(boxTexture, legendBoxStyle);

        boxTexture = MakeTex(15, 15, progressWithinBoundsColor); //create texture with color of progress when it is within bounds
        GUILayout.Label("Progress within bounds", legendLabelStyle);
        GUILayout.Box(boxTexture, legendBoxStyle);

        boxTexture = MakeTex(15, 15, progressOutOfBoundsColor); //create texture with color of progress when it isn't within bounds
        GUILayout.Label("Progress out of bounds", legendLabelStyle);
        GUILayout.Box(boxTexture, legendBoxStyle);

        GUILayout.EndHorizontal();
        GUILayout.EndArea(); //Colors section
        DrawGraphArea(); //Draw graph background
        GUILayout.BeginArea(drawAreaSection);

        DrawGraph(); //Draw all graphs

        GUILayout.EndArea();//Draw Area Section
        GUILayout.BeginArea(sliderSection);

        if (graph.channel[12]._data.Count > drawAreaSection.width) //Use slider when the graphs are too wide
        {
            graphOffset = (int)GUILayout.HorizontalSlider(graphOffset, 0, graph.channel[12]._data.Count - drawAreaSection.width, graphSliderStyle, graphSliderThumbStyle);
        }

        GUILayout.EndArea();//Slider Section
        GUILayout.EndArea(); //Body section       
    }

    //Draws graphs background
    void DrawGraphArea()
    {
        int startpointX = (int)drawAreaSection.x;
        int startpointY = (int)drawAreaSection.y;

        GUI.DrawTexture(drawAreaSection, drawAreaSectionTexture);
    }

    //Draw Graphs
    void DrawGraph()
    {
        //Add colors to channels
        graph.channel[10]._color = minAndMaxColor;
        graph.channel[11]._color = minAndMaxColor;
        graph.channel[12]._color = avarageColor;
        graph.channel[13]._color = progressWithinBoundsColor;

        if (Event.current.type != EventType.Repaint)
            return;

        if (graph.channel[0] == null)
            return;

        int W = (int)drawAreaSection.width;
        int H = (int)drawAreaSection.height;

        int YMin = 0;
        int YMax = 10;
        
        int prevY = 0;

        GL.PushMatrix();
        GL.LoadPixelMatrix();

        GL.Begin(GL.LINES);

        //Draws all graphs except for the progress graph
        for (int chan = 0; chan < Graph.MAX_CHANNELS - 1; chan++)
        {
            Channel C = graph.channel[chan];

            if (C == null)
            {
                Debug.Log("Channel " + chan + " is empty");
            }

            if (!C.isActive)
            {
                continue;
            }

            GL.Color(C._color); //Sets color to plot

            //this gives error in Unity 2020.3 where it freezes unity when no data is found
            for (int x = graphOffset; x < W + graphOffset - 1 && x < graph.channel[12]._data.Count; x++) //Draw all points
            {
                int xPix = x - graphOffset;

                xPix = xPix * 2;

                if (xPix >= 0)
                {
                    float y = C._data[x];

                    float y_01 = Mathf.InverseLerp(YMin, YMax, y);

                    int yPix = H - (int)(y_01 * H);

                    //Fill gaps between the dots
                    if (x != graphOffset)
                    {
                        int differenceBetweenYValues = Mathf.Abs(yPix - prevY);
                        if (differenceBetweenYValues > 1)
                        {
                            int iMax;
                            int jMax;

                            if (differenceBetweenYValues % 2 == 0)
                            {
                                iMax = differenceBetweenYValues / 2;
                                jMax = differenceBetweenYValues / 2;
                            }
                            else
                            {
                                iMax = (differenceBetweenYValues / 2) + 1;
                                jMax = ((differenceBetweenYValues - 1) / 2);
                            }

                            for (int i = 0; i < iMax; i++)
                            {
                                if (yPix - prevY > 0)
                                {
                                    Plot(xPix - 1, prevY + i);
                                }
                                else
                                {
                                    Plot(xPix - 1, prevY - i);
                                }
                            }
                            for (int j = 0; j < jMax; j++)
                            {
                                if (yPix - prevY > 0)
                                {
                                    Plot(xPix, yPix - j);
                                }
                                else
                                {
                                    Plot(xPix, yPix + j);
                                }
                            }
                        }
                    }

                    Plot(xPix, yPix);

                    prevY = yPix;
                }
            }

            prevY = 0;
        }

        //When playing and AI Agent is assessing draw the progress graph
        if (EditorApplication.isPlaying && aiagent.mode == AIAgent.Mode.Assessing)
        {
            if (aiagent.isWithinBounds) //Set to green when the graph is within bounds and to red when it isn't
            {
                graph.channel[13]._color = progressWithinBoundsColor;
            }
            else
            {
                graph.channel[13]._color = progressOutOfBoundsColor;
            }
            GL.Color(graph.channel[13]._color);

            for (int x = graphOffset; x < W + graphOffset - 1 && x < graph.channel[13]._data.Count; x++) //Draw all points
            {
                int xPix = x - graphOffset;
                xPix = xPix * 2;


                if (xPix >= 0)
                {
                    float y = graph.channel[13]._data[x];

                    float y_01 = Mathf.InverseLerp(YMin, YMax, y);

                    int yPix = H - (int)(y_01 * H);

                    //Fill gaps between the dots
                    if (x != graphOffset)
                    {
                        int differenceBetweenYValues = Mathf.Abs(yPix - prevY);
                        if (differenceBetweenYValues > 1)
                        {
                            int iMax;
                            int jMax;

                            if (differenceBetweenYValues % 2 == 0)
                            {
                                iMax = differenceBetweenYValues / 2;
                                jMax = differenceBetweenYValues / 2;
                            }
                            else
                            {
                                iMax = (differenceBetweenYValues / 2) + 1;
                                jMax = ((differenceBetweenYValues - 1) / 2);
                            }

                            for (int i = 0; i < iMax; i++)
                            {
                                if (yPix - prevY > 0)
                                {
                                    Plot(xPix - 1, prevY + i);
                                }
                                else
                                {
                                    Plot(xPix - 1, prevY - i);
                                }
                            }
                            for (int j = 0; j < jMax; j++)
                            {
                                if (yPix - prevY > 0)
                                {
                                    Plot(xPix, yPix - j);
                                }
                                else
                                {
                                    Plot(xPix, yPix + j);
                                }
                            }
                        }
                    }

                    Plot(xPix, yPix);

                    prevY = yPix;
                }
            }

            prevY = 0;
        }

        GL.End();
        GL.PopMatrix();
    }

    void Plot(float x, float y)
    {
        var relativeOffset = (position.width / graph.channel[12]._data.Count) * 0.49f;

        // first line of X
        GL.Vertex3((x * relativeOffset) + 10 - 1, y - 1, 0);
        GL.Vertex3((x * relativeOffset) + 10 + 1, y + 1, 0);

        // second
        GL.Vertex3((x * relativeOffset) + 10 - 1, y + 1, 0);
        GL.Vertex3((x * relativeOffset) + 10 + 1, y - 1, 0);
    }

    //Makes all channels active exept for progress graph
    void SetChannelsActive()
    {
        for (int chan = 0; chan < Graph.MAX_CHANNELS - 1; chan++)
        {
            graph.channel[chan].isActive = true;
        }
    }

    void OnEnable()
    {
        EditorApplication.update += MyDelegate;
    }

    void OnDisable()
    {

        EditorApplication.update -= MyDelegate;
    }

    void MyDelegate()
    {
        Repaint();
    }

    //Make texture for legend
    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }

    //When the agent is lost, find it in the scene
    private void CheckForAgent()
    {
        if (!aiagent)
        {
            GameObject aiSystem = GameObject.Find("AI System");
            if (aiSystem)
            {
                Transform aiagentsContainer = aiSystem.transform.Find("AI Agents");
                if (aiagentsContainer)
                {
                    aiagent = aiagentsContainer.Find(agentName).GetComponent<AIAgent>();
                    graph = aiagent.graph;
                }
            }
        }
    }
}
#endif