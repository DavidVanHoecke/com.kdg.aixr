﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using System.Globalization;
using TMPro;
using System.Threading.Tasks;
using System.Linq;
using UnityEngine.UI;
//using UnityEditor;
using System.Threading;


/// <summary>
/// TO DO:
/// Simplify
/// Add more comment
/// Make bug proof
/// Public => protected
/// </summary>
public class AIAgent : MonoBehaviour
{
    public enum Mode
    {
        GatherTrainingData,
        Assessing,
        Idle //use this when the game hasn't started yet, during cut scenes, or when the game has ended
    }
    public enum Purpose
    {
        training,
        assessing
    }

    [SerializeField]
    public Mode mode = Mode.Idle;

    [Header("Identification")]
    [SerializeField]
    public string identifier;

    [Header("What to learn or assess")]
    [SerializeField]
    bool AutomaticTraining = true;

    [Header("Trajectories and phases")]
    [SerializeField]
    public double ThresholdScaleFactor = 2; //use this to scale the allowed region vertically
    public float virtualTimeDeadZone = 2;

    [Header("File management")]
    [SerializeField]
    public string outputFolderPathName;

    [Header("Events")]
    public UnityEvent OnAboveThreshold;
    public UnityEvent OnWithinThresholds;
    public UnityEvent OnBelowThreshold;
    public UnityEvent OnStartGatheringTrainingData;
    public UnityEvent OnTrainingDataGathered;
    public UnityEvent OnStartTraining;
    public UnityEvent OnTrainingComplete;

    //for AIXR window events
    public delegate void OnStartTrainingEvent();
    public static event OnStartTrainingEvent OnStartTrain;
    public delegate void OnCompleteTrainingEvent();
    public static event OnCompleteTrainingEvent OnCompleteTrain;

    public Graph graph = new Graph();
    public bool isWithinBounds = true;
    string results = string.Empty;

    #region Aux vars
    double valueToTrack = -1; //Raw single value
    double score = -1; //only used during assessing, measurement of valueToTrack compared to avg from training, integrated over time (or other metric)
    double[] bufferOfValuesToTrack;
    int bufferSize = 20;
    int nrOfCurrentTrajectory = -1;
    int nrOfCurrentPhaseInTrajectory = -1;
    double virtualTime = 0;
    double maxVirtualTime = -1;
    int GPIndex = 0;
    List<Vector3> trainedGP = new List<Vector3>();
    double avgValueToTrack = -1;
    double twoSigmaValueToTrack = -1;
    float trackingInterval = 0.5f;
    float trackingTimer = 0f;
    bool notificationAboveSend;
    bool notificationWithinSend = true;
    bool notificationBelowSend;
    List<GP> GPs = new List<GP>();
    int nrOfTrajectories = -1; //max column 0
    double maxTimeStamp = -1; //max column 1
    int nrOfSlices = -1; //2*int (np.size(my_data,0)/nr_of_trajectories)
    List<double> Xnew = new List<double>(); //np.linspace(0, 100, nr_of_slices)[:,None]
    double kernelHeightscale = -1;
    double kernelLengthscale = -1; //20 * (my_data[5, 1] - my_data[4, 1])
    double kernelNoise = 0.25;
    List<double> muCombined = new List<double>();
    List<double> sigmaCombined = new List<double>();
    public Animator animator;
    CanvasGroup AIStatusPanel;
    TextMeshProUGUI AIStatusTextCaption;
    TextMeshProUGUI AIStatusTextProgress;
    Slider slider;
    int totalTrajectories, currentTrajectory, totalTrainingParts, currentTrainingParts;
    TextMeshProUGUI detailsText;
    string details;
    string assessingDataFilePath;
    string trainingDataFilePath;
    bool trainingFileIsLocked;
    bool updateTrainingProgress;
    bool duplicateTimestampReceived;
    bool fadeIn = false;
    float fadeInDelay = 0.5f;

    public bool isTraining;
    #endregion

    private void Awake()
    {
        CheckSceneObjects();

        if (!animator)
        {
            animator = GetComponent<Animator>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Init();
        InitAIUI();

        switch (mode)
        {
            case Mode.GatherTrainingData: //TODO try catch
                OnStartGatheringTrainingData.Invoke();

                break;
            case Mode.Assessing:
                GPIndex = 0;
                LoadTrainedGP();
                score = 0;
                break;
            default:
                break;
        }

        trackingTimer = 0f;

        StartNewTrajectory();

        SetGraphStuff();

        ResetBufferValuesToTrack();
    }

    public void Init()
    {
        // make sure all conversions are done independent of CurrentCultureInfo
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

        CheckSceneObjects();

        InitFilesAndPaths();

        InitEvents();
    }

    private void CheckSceneObjects()
    {
        var msgResetter = "You must have at least 1 active and enabled TrajectoryResetter in the scene.";
        var msgResetterAIAgent = "A TrajectoryResetter in the scene must have at least 1 active and enabled AI Agent in its AgentsToReset list.";

        // if in gathering mode
        if (mode == Mode.GatherTrainingData)
        {
            var trajectoryResetters = GameObject.FindObjectsOfType<TrajectoryResetter>();

            if (trajectoryResetters == null || trajectoryResetters.Length == 0)
                throw new InvalidOperationException(msgResetter);
            else
            {
                foreach (var trajectoryResetter in trajectoryResetters)
                {
                    if (trajectoryResetter.agentsToReset == null || trajectoryResetter.agentsToReset.Length == 0)
                        throw new InvalidOperationException(msgResetterAIAgent);
                    else
                    {
                        foreach (var aiAgent in trajectoryResetter.agentsToReset)
                        {
                            if (aiAgent == null)
                                throw new InvalidOperationException(msgResetterAIAgent);
                        }
                    }
                }
            }
        }
    }

    private void InitFilesAndPaths()
    {
        outputFolderPathName = Path.Combine(Application.persistentDataPath, "AIAgent Output");
        if (string.IsNullOrEmpty(identifier))
            throw new InvalidOperationException("You must set an identifier for this AIAgent instance.");

        try
        {
            if (!Directory.Exists(outputFolderPathName))
                Directory.CreateDirectory(outputFolderPathName);

            trainingDataFilePath = EnsureTextFileExistsAndClearContents(CreateOutputDataFileName(Purpose.training));
            assessingDataFilePath = System.IO.Path.Combine(outputFolderPathName, CreateOutputDataFileName(Purpose.assessing));
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private string CreateOutputDataFileName(Purpose purpose)
    {
        if (purpose == Purpose.training)
        {
            var completedTrainingFiles = GetCompletedTrainingFilePaths();
            nrOfCurrentTrajectory = 0 + completedTrainingFiles.Count;

            return string.Format("{0}-{1}-{2}-temp.csv", identifier, Purpose.training, nrOfCurrentTrajectory.ToString().PadLeft(3, '0'));
        }

        return string.Format("{0}-{1}.csv", identifier, Purpose.assessing);
    }

    private List<string> GetCompletedTrainingFilePaths()
    {
        return Directory.GetFiles(outputFolderPathName, string.Format("{0}-{1}-*", identifier, Purpose.training)).Where(name => !name.Contains("temp")).ToList();
    }

    private string EnsureTextFileExistsAndClearContents(string fileName)
    {
        string path = System.IO.Path.Combine(outputFolderPathName, fileName);
        if (File.Exists(path))
            File.WriteAllText(path, string.Empty);
        else
            using (var sr = new StreamWriter(path)) { }

        return path;
    }

    private void FinalizeTemporaryTrainingFile()
    {
        trainingFileIsLocked = true;
        File.Move(trainingDataFilePath, trainingDataFilePath.Replace("-temp", string.Empty));
        trainingDataFilePath = EnsureTextFileExistsAndClearContents(CreateOutputDataFileName(Purpose.training));
        trainingFileIsLocked = false;
    }

    private void InitAIUI()
    {
        AIStatusPanel = GetComponentInChildren<CanvasGroup>();
        var textFields = GetComponentsInChildren<TextMeshProUGUI>();
        AIStatusTextCaption = textFields.Where(component => component.name == "Caption").FirstOrDefault();
        AIStatusTextProgress = textFields.Where(component => component.name == "Progress").FirstOrDefault();
        detailsText = textFields.Where(component => component.name == "Details").FirstOrDefault();
        slider = GetComponentInChildren<Slider>();
    }

    private void InitEvents()
    {
        if (OnStartGatheringTrainingData == null)
            OnStartGatheringTrainingData = new UnityEvent();

        if (OnTrainingDataGathered == null)
            OnTrainingDataGathered = new UnityEvent();

        if (OnStartTraining == null)
            OnStartTraining = new UnityEvent();


        if (OnTrainingComplete == null)
            OnTrainingComplete = new UnityEvent();

        OnStartTraining.AddListener(StartTrainingListener);
        OnTrainingComplete.AddListener(CompletedTrainingListener);
    }

    private void StartTrainingListener()
    {
        slider.value = 0f;

        fadeIn = true;

        AIStatusTextCaption.text = "Training AI, stay in play mode...";
    }

    private void CompletedTrainingListener()
    {
        if (isTraining)
            AIStatusTextCaption.text = "AI currently training, cannot start another training process.";
        else
        {
            AIStatusTextCaption.text = "AI training complete, you can stop play mode or start another trajectory...";
            if (this.gameObject.activeInHierarchy)
            {
                StartCoroutine(WaitAndFadeOut());
            }
            
        }
    }

    System.Collections.IEnumerator WaitAndFadeOut()
    {
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(5);

        fadeIn = false;
    }

    private void TrainingProgressUpdate()
    {
        AIStatusTextProgress.text = string.Format("Training trajectory {0}/{1}", currentTrajectory, totalTrajectories);
        slider.value = Mathf.Lerp(slider.value, currentTrainingParts / (totalTrajectories * 5f), 2f * Time.deltaTime);
        detailsText.text = details;

        if (slider.value == 1)
        {
            updateTrainingProgress = false;
            //AIStatusTextProgress.DOFade(0f, 0.5f);
            AIStatusTextProgress.alpha = 0.0f;
        }
    }

    void ResetBufferValuesToTrack()
    {
        bufferOfValuesToTrack = new double[bufferSize];
        for (int i = 0; i < bufferOfValuesToTrack.Length; i++)
            bufferOfValuesToTrack[i] = 50;
    }

    void SetGraphStuff()
    {

        graph.channel[0].isActive = true;
        graph.channel[1].isActive = true;
        graph.channel[2].isActive = true;
        graph.channel[3].isActive = true;
        graph.channel[4].isActive = true;
        graph.channel[5].isActive = true;
        graph.channel[6].isActive = true;
        graph.channel[7].isActive = true;
        graph.channel[8].isActive = true;
        graph.channel[9].isActive = true;
        graph.channel[10].isActive = true;
        graph.channel[11].isActive = true;
        graph.channel[12].isActive = true;
        graph.channel[13].isActive = true;
    }


    // Update is called once per frame

    void Update()
    {
        // do fades
        HandlePanelFades();

        virtualTime += Time.deltaTime;

        if (updateTrainingProgress)
            TrainingProgressUpdate();

        switch (mode)
        {
            case Mode.GatherTrainingData:
                GatherTrainingData();
                break;
            case Mode.Assessing:
                Assess();
                break;
            default:
                break;
        }
    }

    private void HandlePanelFades()
    {
        if (fadeIn)
        {
            AIStatusTextProgress.alpha += Time.deltaTime / fadeInDelay;
            AIStatusPanel.alpha += Time.deltaTime / fadeInDelay;
        }
        else
        {
            AIStatusTextProgress.alpha -= Time.deltaTime / fadeInDelay;
            AIStatusPanel.alpha -= Time.deltaTime / fadeInDelay;
        }
    }

    public async Task TrainAsync()
    {
        var completedTrainingFilePaths = GetCompletedTrainingFilePaths();
        totalTrajectories = currentTrajectory = 0;
        if (completedTrainingFilePaths.Count > 0)
        {
            OnStartTraining.Invoke();
            if(OnStartTrain != null)
                OnStartTrain();
            if (isTraining)
            {
                OnTrainingComplete.Invoke();
                return;
            }
            isTraining = true;

            
        }

        GPs.Clear();

        Debug.Log("Train on " + trainingDataFilePath);

        duplicateTimestampReceived = false;

        await Task.Run(() =>
        {
            //Read data from csv that was written when mode was gathering data
            var timestampsList = new List<double>();
            List<double[]> data = new List<double[]>();
            double timeStamp;
            foreach (var currentTrainingDataFilePath in completedTrainingFilePaths)
            {
                maxTimeStamp = 0;
                using (StreamReader file = new StreamReader(currentTrainingDataFilePath, true))
                {
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        string[] words = line.Split(',');
                        double[] wordsAsDoubles = new double[words.Length];
                        for (int i = 0; i < words.Length; i++)
                            wordsAsDoubles[i] = Convert.ToDouble(words[i]);

                        // check for duplicate timestamps
                        timeStamp = wordsAsDoubles[1];
                        if (timestampsList.Contains(timeStamp))
                        {
                            duplicateTimestampReceived = true;
                            // TODO, reduce floating point to 4 and add 0.000066666 so it's easily found in log files
                            wordsAsDoubles[1] += 0.00000000001;
                        }
                        else
                            timestampsList.Add(wordsAsDoubles[1]);

                        data.Add(wordsAsDoubles);

                        if (wordsAsDoubles[1] > maxTimeStamp)
                            maxTimeStamp = wordsAsDoubles[1];
                    }
                }
                timestampsList.Clear();
            }

            ExpandData(data);

            //Use data for parameters
            nrOfTrajectories = (int)data[data.Count - 1][0] + 1; //max column 0, last trajectory is the bogus one       
            nrOfSlices = data.Count / nrOfTrajectories; // was originally multiplied by 2 to increase graph resolution

            Xnew = new List<double>(); //np.linspace(0, max_timestamp, nr_of_slices)[:,None]
            for (int i = 0; i < nrOfSlices; i++)
                Xnew.Add(i * (double)maxTimeStamp / (double)nrOfSlices);

            //Hyperparameters
            kernelHeightscale = 1;
            kernelLengthscale = 10;// Math.Sqrt(10); //20 * (my_data[5, 1] - my_data[4, 1])
            kernelNoise = 0.1;

            Debug.Log("Start GP construct");
            //Construct GP for every trajectory
            for (int i = 0; i < nrOfTrajectories; i++)
            {
                List<double> xTrain = new List<double>();
                List<double> yTrain = new List<double>();
                for (int j = 0; j < data.Count; j++)
                {
                    if (data[j][0] == i)
                    {
                        xTrain.Add(data[j][1]);
                        yTrain.Add(data[j][3]);
                    }
                }
                GP gp = new GP();
                gp.XTrain = xTrain;
                gp.YTrain = yTrain;
                gp.XTest = Xnew;
                gp.kernelHeighScale = kernelHeightscale;
                gp.kernelLengthScale = kernelLengthscale;
                gp.kernelNoise = kernelNoise;
                GPs.Add(gp);
            }

            Debug.Log("Start GP train");
            //Train every GP
            totalTrajectories = GPs.Count;
            totalTrainingParts = totalTrajectories * 5;
            currentTrainingParts = 0;
            updateTrainingProgress = true;
            for (int i = 0; i < GPs.Count; i++)
            {
                details = "Calculate K";
                currentTrajectory = i + 1;
                GPs[i].CalculateK();
                currentTrainingParts++;
                details = "Calculate Ks";
                GPs[i].CalculateKs();
                currentTrainingParts++;
                details = "Calculate Kss";
                GPs[i].CalculateKss();
                currentTrainingParts++;
                details = "Calculate Mu";
                GPs[i].CalculateMu();
                currentTrainingParts++;
                details = "Calculate Sigma";
                GPs[i].CalculateSigma();
                currentTrainingParts++;
            }
            details = string.Empty;

            Debug.Log("Start GP combine");
            //Combine all GPs
            for (int i = 0; i < nrOfSlices; i++)
            {
                double mu = 0;
                for (int j = 0; j < nrOfTrajectories; j++)
                    mu += GPs[j].GetMuAt(i);

                mu /= (double)(nrOfTrajectories);
                muCombined.Add(mu);
                double sigma = 0;
                for (int j = 0; j < nrOfTrajectories; j++)
                    sigma += GPs[j].GetSigmaAt(i) + (GPs[j].GetMuAt(i) * GPs[j].GetMuAt(i));

                sigma /= (double)(nrOfTrajectories);
                sigma -= (mu * mu);
                sigmaCombined.Add(sigma);
            }

            Debug.Log("Start GP write output file");
            //Write data from combined GP to csv
            using (System.IO.FileStream fs = System.IO.File.Create(assessingDataFilePath)) { }
            using (System.IO.StreamWriter fs = new System.IO.StreamWriter(assessingDataFilePath, false))
            {
                string s = "";
                for (int i = 0; i < Xnew.Count; i++)
                    s += Xnew[i].ToString() + "," + muCombined[i].ToString() + "," + sigmaCombined[i].ToString() + "\n";

                fs.Write(s);
            }

        });

        // TODO: refactor
        if (duplicateTimestampReceived)
            Debug.LogWarning("Duplicate timestamp received.");

        isTraining = false;
        OnTrainingComplete.Invoke();
        if (OnCompleteTrain != null)
        {
            OnCompleteTrain();
        }
    }



    public void LoadTrainedGP()
    {
        using (StreamReader file = new StreamReader(assessingDataFilePath, true))
        {
            string line;
            while ((line = file.ReadLine()) != null)
            {
                //Debug.Log(line);
                string[] words = line.Split(',');
                float x = 0;
                float y = 0;
                float z = 0;
                float.TryParse(words[0], NumberStyles.Any, CultureInfo.InvariantCulture, out x);
                float.TryParse(words[1], NumberStyles.Any, CultureInfo.InvariantCulture, out y);
                float.TryParse(words[2], NumberStyles.Any, CultureInfo.InvariantCulture, out z);
                
                Vector3 v = new Vector3(x, y, z);
                graph.channel[10].FeedReversed((float)(y + ThresholdScaleFactor * 2 * Math.Sqrt(Math.Abs(z))));
                graph.channel[11].FeedReversed((float)(y - ThresholdScaleFactor * 2 * Math.Sqrt(Math.Abs(z))));
                graph.channel[12].FeedReversed((float)y);
                trainedGP.Add(v);

                for (int i = 0; i < graph.channel.Length - 4; i++)
                    graph.channel[i].FeedReversed((float)i + 1);
            }
        }
    }

    /// <summary>
    /// Training of AI-agent, logging of training data
    /// </summary>
    public void GatherTrainingData()
    {
        trackingTimer += Time.deltaTime;
        if (trackingTimer > trackingInterval)
        {
            trackingTimer = 0;
            UpdateTrainingDataFile(GenerateStringForFile());
        }
    }

    /// <summary>
    /// Use the trained AI-agent to assess the trainee
    /// </summary>
    public void Assess()
    {
        while (virtualTime > trainedGP[GPIndex].x && GPIndex < trainedGP.Count - 1)
        {
            GPIndex++;
            graph.channel[13].FeedReversed((float)valueToTrack);
            graph.currValue = valueToTrack;
            graph.avgValue = avgValueToTrack;
        }
        avgValueToTrack = trainedGP[GPIndex].y;
        twoSigmaValueToTrack = ThresholdScaleFactor * 2 * Math.Sqrt(trainedGP[GPIndex].z);

        score += valueToTrack - avgValueToTrack;

        ManageEvents();
    }

    public void ManageEvents()
    {
        if (virtualTime > virtualTimeDeadZone)
        {
            //Debug.LogWarning(valueToTrack + " " + (avgValueToTrack + twoSigmaValueToTrack));
            if (valueToTrack > avgValueToTrack + twoSigmaValueToTrack && !notificationAboveSend)
            {
                Debug.LogWarning("valueToTrack > avgValueToTrack");

#if UNITY_EDITOR
                GraphDuringTraining.withinRange = false;
                isWithinBounds = false;
#endif
                OnAboveThreshold.Invoke();
                notificationAboveSend = true;
                notificationWithinSend = false;
                notificationBelowSend = false;
            }
            else if (valueToTrack > avgValueToTrack - twoSigmaValueToTrack && valueToTrack < avgValueToTrack + twoSigmaValueToTrack && !notificationWithinSend)
            {
                Debug.LogWarning("valueToTrack within range again");

#if UNITY_EDITOR
                GraphDuringTraining.withinRange = true;
                isWithinBounds = true;
#endif
                OnWithinThresholds.Invoke();
                notificationAboveSend = false;
                notificationWithinSend = true;
                notificationBelowSend = false;
            }
            else if (valueToTrack < avgValueToTrack - twoSigmaValueToTrack && !notificationBelowSend)
            {
                Debug.LogWarning("valueToTrack < avgValueToTrack");

#if UNITY_EDITOR
                GraphDuringTraining.withinRange = false;
                isWithinBounds = false;
#endif
                OnBelowThreshold.Invoke();
                notificationAboveSend = false;
                notificationWithinSend = false;
                notificationBelowSend = true;
            }
        }
    }

    public void AddToValueToTrack(float valueToAdd)
    {
        valueToTrack += valueToAdd;
        Debug.Log("Add " + valueToAdd + " to valueToTrack. New valueToTrack: " + valueToTrack);
    }

    public void GoToNextPhase(float newVirtualTime, float newValueToTrack)
    {
        Debug.Log("Going to phase " + (nrOfCurrentPhaseInTrajectory + 1));

        if (nrOfCurrentPhaseInTrajectory >= 0)
            UpdateTrainingDataFile(GeneratePaddingForFile(newVirtualTime)); //use padding to ensure that the next phase starts at the same virtual time, no matter what happened before this phase

        virtualTime = newVirtualTime;
        Debug.Log("Set virtualTime to " + newVirtualTime + ".");

        valueToTrack = newValueToTrack;
        Debug.Log("Set valueToTrack to " + newValueToTrack + ".");

        ResetBufferValuesToTrack();

        nrOfCurrentPhaseInTrajectory++;
        StartNewPhaseInTrajectory();
    }

    public void StartNewTrajectory(bool calledFromTrajectoryResetter = false)
    {
        if (nrOfCurrentTrajectory >= 0 && mode == Mode.GatherTrainingData) //first time starting new trajectory this could be at -1, in that case no trajectory has been started, no padding needed, only needed at the end of a trajectory
            UpdateTrainingDataFile(GeneratePaddingForFile(maxVirtualTime));

        //Debug.Log("Starting trajectory " + (metaData.TrajectoryIndex + 1));

        if (calledFromTrajectoryResetter)
        {
            FinalizeTemporaryTrainingFile();
            OnTrainingDataGathered.Invoke();
        }

        if (mode == Mode.GatherTrainingData && AutomaticTraining)
            TrainAsync().ConfigureAwait(false);

        virtualTime = 0;
        nrOfCurrentPhaseInTrajectory = 0;
        valueToTrack = 0;
        if (animator && nrOfCurrentTrajectory > 0)
            animator.SetTrigger("Reset");

        trackingTimer = 0;

        StartNewPhaseInTrajectory();
    }

    public void StartNewPhaseInTrajectory()
    {
        if (mode == Mode.Assessing)
            ManageEvents();
    }

    public void UpdateTrainingDataFile(string outputString)
    {
        // Append new text to an existing file. The using statement automatically flushes AND CLOSES the stream and calls IDisposable.Dispose on the stream object.
        if (trainingFileIsLocked)
            return;

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(trainingDataFilePath, true))
        {
            file.Write(outputString);
        }
    }

    public string GenerateStringForFile()
    {
        string s = "";
        s += nrOfCurrentTrajectory;
        s += ",";
        s += virtualTime;
        s += ",";
        s += nrOfCurrentPhaseInTrajectory;
        s += ",";
        s += valueToTrack;
        s += "\n";
        return s;
    }

    public string GeneratePaddingForFile(double newVirtualTime)
    {
        string s = "";
        while (virtualTime < newVirtualTime)
        {
            s += GenerateStringForFile();
            virtualTime += trackingInterval;
        }
        return s;
    }

    public void TrainAIAgents()
    {
        results = "Starting AI Agents training";

        // get all AIAgent related csv's from Application.persistentDataPath + "AIAgent Output"
        var allFoundAIFiles = Directory.GetFiles(Path.Combine(Application.persistentDataPath, "AIAgent Output"), "*.csv");

        // iterate over all filenames to get individual AIAgent identifier strings
        var allAIAgentIdentifiers = new List<string>();
        allFoundAIFiles.ToList().ForEach(fileName =>
        {
            var identifier = Path.GetFileName(fileName).Split('-')[0];
            if (!allAIAgentIdentifiers.Contains(identifier))
                allAIAgentIdentifiers.Add(identifier);
        });

        // warn user if no identifiers / agents found and exit method
        if (allAIAgentIdentifiers.Count == 0)
        {
            results += "\nNo AIAgents found.";
            return;
        }

        // iterate over all identifiers,...
        var i = 0;
        allAIAgentIdentifiers.ForEach(async identifier =>
        {
            results += "\n... AIAgent found: \"" + identifier + "\"";

            // ... create an AIAgent per identifier while passing that identifier to the AIAGent, 
            var agent = new AIAgent { identifier = identifier };

            // ... initialize the AIAgent,
            agent.Init();

            // ... run Train method on worker thread but await results
            await agent.TrainAsync().ConfigureAwait(true);

            // ... and show progress to user
            results += "\n... AIAgent trained: \"" + identifier + "\"";
            i++;
            if (i == allAIAgentIdentifiers.Count) // if all agents have run, notify user as well
                results += "\nAll AIAgents trained";
        });
    }

    private List<double[]> ExpandData(List<double[]> data)
    {
        double highestTrajectory = data[data.Count() - 1][0];

        List<double[]>[] Trajectories = new List<double[]>[(int)highestTrajectory + 1];

        foreach (double[] dataEntry in data)
        {
            if (Trajectories[(int)dataEntry[0]] == null)
            {
                Trajectories[(int)dataEntry[0]] = new List<double[]>();
            }
            Trajectories[(int)dataEntry[0]].Add(dataEntry);
        }

        int longestTrajectory = 0;

        for (int i = 1; i < Trajectories.Length; i++)
        {
            if (Trajectories[i].Last<double[]>()[1] > Trajectories[longestTrajectory].Last<double[]>()[1])
            {
                longestTrajectory = i;
            }
        }

        for (int i = 0; i < Trajectories.Length; i++)
        {
            if (longestTrajectory != i)
            {
                for (int j = Trajectories[i].Count; j < Trajectories[longestTrajectory].Count; j++)
                {
                    Trajectories[i].Add(new double[4] { Trajectories[i][Trajectories[i].Count - 1][0], Trajectories[longestTrajectory][j][1], Trajectories[i][Trajectories[i].Count - 1][2], Trajectories[i][Trajectories[i].Count - 1][3] });
                }
            }
        }

        data = new List<double[]>();

        for (int i = 0; i < Trajectories.Length; i++)
        {
            for (int j = 0; j < Trajectories[longestTrajectory].Count; j++)
            {
                data.Add(Trajectories[i][j]);
            }
        }

        return data;
    }
}
