﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Checkpoint : MonoBehaviour
{
    public enum CheckpointMode
    {
        EnableAgent,
        standard,
        nextPhase,
        DisableAgent
    }

    public CheckpointMode checkpointMode = CheckpointMode.standard;

    public AIAgent aiAgent; //could be multiple agents in scene so fill in in inspector manually
    public float AddValueToValueToTrack = 0;
    public float ValueToTrackValueToResetNextPhaseTo = 0;
    public float VirtualTimeToResetNextPhaseTo = 0;

    [SerializeField] private UnityEvent onTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.name.Contains("FPSController") || other.transform.name.Contains("Player"))
        {

            if (checkpointMode == CheckpointMode.EnableAgent)
            {
                aiAgent.enabled = true;
            }
            else if (checkpointMode == CheckpointMode.nextPhase)
            {
                //jump forward in time so the new phase starts at the same (virtual) time no matter how fast or slow the player played before this checkpoint
                aiAgent.GoToNextPhase(VirtualTimeToResetNextPhaseTo, ValueToTrackValueToResetNextPhaseTo);
            }
            else if(checkpointMode == CheckpointMode.DisableAgent)
            {
                aiAgent.enabled = false;
            }

            aiAgent.AddToValueToTrack(AddValueToValueToTrack);
            onTrigger.Invoke();
            gameObject.SetActive(false);
        }
    }
}
