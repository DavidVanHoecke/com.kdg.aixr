using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MouseLook : MonoBehaviour
{
    private float mouseSensitivity = 10f;
    public Transform playerBody;
    float xRotation = 0f;
    float mouseY; 
    float mouseX;
    public bool GetPlayerInput = true;
    [SerializeField]
    private InputAction turning;

    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
    }


    //public void Turn(InputAction.CallbackContext value)
    //{
    //    Vector2 mousemovement = value.ReadValue<Vector2>();
    //    mouseX = mousemovement.x * mouseSensitivity * Time.deltaTime;
    //    mouseY = mousemovement.y * mouseSensitivity * Time.deltaTime;
    //    xRotation -= mouseY;
    //    xRotation = Mathf.Clamp(xRotation, -90f, 90f);
    //    transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
    //    playerBody.Rotate(Vector3.up * mouseX);
    //}

    private void OnEnable()
    {
        turning.Enable();
    }

    private void OnDisable()
    {
        turning.Disable();

    }

    public void Turning()
    {

        mouseX = turning.ReadValue<Vector2>().x * mouseSensitivity * Time.deltaTime;
        mouseY = turning.ReadValue<Vector2>().y * mouseSensitivity * Time.deltaTime;
        Debug.Log(mouseX);
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }

    void Update()
    {
        Turning();
        //if(Input.GetKeyDown(KeyCode.Escape))
        //{
        //    GetPlayerInput = !GetPlayerInput;
        //}
        //if(GetPlayerInput)
        //{
        //    Cursor.lockState = CursorLockMode.Locked;
        //    //mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        //    //mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        //    xRotation -= mouseY;
        //    xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        //    transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        //    playerBody.Rotate(Vector3.up * mouseX);
        //}
        //else
        //{
        //    Cursor.lockState = CursorLockMode.None;
        //}




    }

}
