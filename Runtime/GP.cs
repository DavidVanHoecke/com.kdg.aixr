﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathNet.Numerics.LinearAlgebra;
using System;
using UnityEngine.Events;


[Serializable]
public class GP //: MonoBehaviour
{
    public List<double> XTrain;
    public List<double> YTrain;
    public List<double> XTest;

    public Matrix<double> Mu;
    public Vector<double> SigmaDiagonal;

    public double kernelHeighScale = 1;
    public double minKernelHeightScale = 0.05;
    public double maxKernelHeightScale = 1;

    public double kernelLengthScale = 1;
    public double minKernelLengthScale = 0.1;
    public double maxKernelLengthScale = 2;

    public double kernelNoise = 0;
    public double minKernelNoise = 0;
    public double maxKernelNoise = 0.1;

    #region aux vars cached for performance reasons

    Matrix<double> I;
    public Matrix<double> K;
    public Matrix<double> KforHyperOpt;
    public Matrix<double> Ks;
    public Matrix<double> Kss;
    public Vector<double> KssDiagonal;
    public Matrix<double> L;
    public Matrix<double> LT;
    public Matrix<double> alpha;
    public Matrix<double> v;
    public Matrix<double> KInverse;
    public Matrix<double> KsT;
    public Matrix<double> KInverseKsT;
    public Matrix<double> KsKInverseKsT;
    public Matrix<double> LInverseKsT;
    public Matrix<double> LInverseKsTT;
    public Vector<double> KsKinverseKsTOnlyDiagonal;

    #endregion

    public Kernel kernel = Kernel.SE;
    public enum Kernel
    {
        SE,
        RQ,
        Matern12,
        Matern32,
        Matern52,
    }


    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CalculateK() //aka training
    {
        switch (kernel)
        {
            case Kernel.SE:
                K = SE(XTrain, XTrain);
                break;
            case Kernel.RQ:
                break;
            case Kernel.Matern12:
                break;
            case Kernel.Matern32:
                break;
            case Kernel.Matern52:
                break;
            default:
                break;
        }
        //Debug.Log(K);
    }

    public void CalculateKs()
    {
        switch (kernel)
        {
            case Kernel.SE:
                Ks = SE(XTest, XTrain);
                break;
            case Kernel.RQ:
                break;
            case Kernel.Matern12:
                break;
            case Kernel.Matern32:
                break;
            case Kernel.Matern52:
                break;
            default:
                break;
        }
        //Debug.Log(Ks);
    }

    public void CalculateKss()
    {
        CalculateKssDiagonal();
    }


    private void CalculateKssDiagonal()
    {
        switch (kernel)
        {
            case Kernel.SE:
                KssDiagonal = SEDiagonal(XTest); //for kss
                break;
            case Kernel.RQ:
                break;
            case Kernel.Matern12:
                break;
            case Kernel.Matern32:
                break;
            case Kernel.Matern52:
                break;
            default:
                break;
        }
        //Debug.Log(Kss);
    }

    public void CalculateMu()
    {
        Matrix<double> YTrainAsMatrix = Matrix<double>.Build.Dense(YTrain.Count, 1);
        for (int i = 0; i < YTrain.Count; i++)
        {
            YTrainAsMatrix[i, 0] = YTrain[i];
        }

        try
        {
            //Cholesky way, abandoned when using a kd-tree, matrix not always positive definite
            I = Matrix<double>.Build.DenseIdentity(XTrain.Count);
            L = (K + kernelNoise * I).Cholesky().Factor;
            LT = L.Transpose();
            alpha = LT.Solve(L.Solve(YTrainAsMatrix));
            Mu = Ks * alpha;
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
        
    }


    public void CalculateSigma()
    {
        CalculateSigmaDiagonal();
    }

    private void CalculateSigmaDiagonal()
    {
        KsKinverseKsTOnlyDiagonal = Vector<double>.Build.Dense(KssDiagonal.Count);

        //Quora way with Cholesky       
        v = L.Solve(Ks.Transpose());
        for (int i = 0; i < KsKinverseKsTOnlyDiagonal.Count; i++)
        {
            KsKinverseKsTOnlyDiagonal[i] = v.Column(i) * v.Column(i);
        }
        //Combine Kss and KsAKsTOnlyDiagonal, but only diagonal
        SigmaDiagonal = Vector<double>.Build.Dense(KssDiagonal.Count);
        for (int i = 0; i < SigmaDiagonal.Count; i++)
        {
            SigmaDiagonal[i] = KssDiagonal[i] - KsKinverseKsTOnlyDiagonal[i];
        }
    }

    public double GetMuAt(int index)
    {
        return Mu[index, 0];
    }
    public double GetSigmaAt(int index)
    {
        double sigmaValueToReturn = 0;
        sigmaValueToReturn = SigmaDiagonal[index];
        return sigmaValueToReturn;
    }

    public Matrix<double> SE(List<double> X, List<double> XPrime)
    {
        Matrix<double> k = Matrix<double>.Build.Dense(X.Count, XPrime.Count);
        for (int i = 0; i < X.Count; i++)
        {
            int lowerLimitForJ = 0;
            if (X.Count == XPrime.Count) //so for square matrices like e.g. K
            {
                lowerLimitForJ = i;
            }
            for (int j = lowerLimitForJ; j < XPrime.Count; j++)
            {
                if (X[i] == XPrime[j]) //distance will be zero
                {
                    k[i, j] = kernelHeighScale * kernelHeighScale;
                }
                else
                {
                    double d = Math.Abs(X[i] - XPrime[j]);
                    k[i, j] = Math.Pow(kernelHeighScale, 2) * Math.Exp(-0.5d * Math.Pow(d, 2) / (Math.Pow(kernelLengthScale, 2)));
                    
                    if (X.Count == XPrime.Count) //so for square matrices like e.g. K
                    {
                        k[j, i] = k[i, j];  //only when a square and using symmetry 
                    }
                }
            }
        }        
        return k;
    }
    public Vector<double> SEDiagonal(List<double> X)
    {
        return Vector<double>.Build.Dense(X.Count, Math.Pow(kernelHeighScale, 2)); ;
    }

}



