﻿//http://wiki.unity3d.com/index.php?title=EditorGraphWindow&oldid=14810

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Graph
    {
        public static float YMin = 0, YMax = 10;

        public const int MAX_HISTORY = 2048; //GraphDurningTraining
        public const int MAX_CHANNELS = 14;

        public Channel[] channel = new Channel[MAX_CHANNELS];

        public double currValue; //GraphDurningTraining
        public double avgValue; //GraphDurningTraining

        public Graph()
        {
            channel[10] = new Channel(Color.cyan); //upper
            channel[11] = new Channel(Color.cyan); //lower
            channel[12] = new Channel(Color.blue); //avg
            channel[13] = new Channel(Color.green); //current player value

            channel[0] = new Channel(Color.grey);
            channel[1] = new Channel(Color.grey);
            channel[2] = new Channel(Color.grey);
            channel[3] = new Channel(Color.grey);
            channel[4] = new Channel(Color.grey);
            channel[5] = new Channel(Color.grey);
            channel[6] = new Channel(Color.grey);
            channel[7] = new Channel(Color.grey);
            channel[8] = new Channel(Color.grey);
            channel[9] = new Channel(Color.grey);

        }

    }