﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Finish : MonoBehaviour
{
    public Transform player;
    public TextMeshProUGUI logText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if (other.transform == player.transform)
        {
            logText.text += "You reached the finish. Well done." + '\n';
            gameObject.SetActive(false);
        }
    }
}
