﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// The trajectory (a run of of the training from start to end), is reset during training of the AI when either the end point, i.e. last checkpoint,
/// of the path is reached of when the last state of the state machine in the animator is reached.
/// </summary>
public class TrajectoryResetter : MonoBehaviour
{
    public UnityEvent OnTrajectoryReset;

    public AIAgent[] agentsToReset;

    public Transform player;
    public Vector3 initPlayerPosition;
    public Quaternion initPlayerRotation; //other scripts like e.g. FPS from standard assets adjust this rotation according to mouse input
    public CharacterController cc;

    public Transform objectsToReset;
    Vector3[] objectPositions;
    Quaternion[] objectRotations;

    // Start is called before the first frame update
    void Start()
    {
        if (OnTrajectoryReset == null)
            OnTrajectoryReset = new UnityEvent();

        if (player)
        {
            if (player.GetComponent<CharacterController>())
            {
                cc = player.GetComponent<CharacterController>();
            }
        }

        if (agentsToReset[0].mode == AIAgent.Mode.Assessing) //just in case
        {
            gameObject.SetActive(false);
        }

        if (player)
        {
            initPlayerPosition = player.localPosition;
            initPlayerRotation = player.localRotation;
        }

        if (objectsToReset)
        {
            objectPositions = new Vector3[objectsToReset.childCount];
            objectRotations = new Quaternion[objectsToReset.childCount];
            for (int i = 0; i < objectsToReset.childCount; i++)
            {
                objectPositions[i] = objectsToReset.GetChild(i).position;
                objectRotations[i] = objectsToReset.GetChild(i).rotation;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetTrajectory()
    {
        Debug.Log("Resetting the trajectory");

        ResetAgents();
        ResetObjects();

        if (player)
        {
            if (cc)
            {
                player.GetComponent<CharacterController>().enabled = false;
            }
            player.localPosition = initPlayerPosition;
            player.localRotation = initPlayerRotation;
            if (cc)
            {
                player.GetComponent<CharacterController>().enabled = true;
            }
        }

        OnTrajectoryReset.Invoke();
    }

    public void ResetAgents()
    {
        for (int i = 0; i < agentsToReset.Length; i++)
        {
            agentsToReset[i].StartNewTrajectory(true);
        }
    }

    public void ResetObjects()
    {
        if (objectsToReset)
        {
            //Debug.Log("objectsToReset found");
            objectsToReset.SendMessage("Reset", SendMessageOptions.DontRequireReceiver);
            for (int i = 0; i < objectsToReset.childCount; i++)
            {
                objectsToReset.GetChild(i).gameObject.SetActive(true);
                objectsToReset.GetChild(i).position = objectPositions[i];
                objectsToReset.GetChild(i).rotation = objectRotations[i];
                objectsToReset.GetChild(i).SendMessage("Reset", SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            //this.gameObject.SetActive(false);
            ResetTrajectory();
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            ResetTrajectory();
        }
    }
}
